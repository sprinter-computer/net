// SprinterNet v1.0.0
function validate(a) {
 var i,c,f;
 var fontred = "<font color=#FF0000><b>";
 var fontend = "</b></font>";
 if(a < 2) {
   var user = document.getElementById("user").value;
   if(user.length < 3) {
     document.getElementById("msg").innerHTML = fontred + err1 + fontend;
     return false;
   }
   for(i=0; i < user.length; i++) {
     c = user.charCodeAt(i);
     if(c == 0x20) {
       document.getElementById("msg").innerHTML = fontred + err2 + fontend;
       return false;
     }
     if(!((c >= 0x30 && c <= 0x39)||(c >= 0x41 && c <= 0x5A)||(c >= 0x61 && c <= 0x7A)||(c == 0x5F))) {
       document.getElementById("msg").innerHTML = fontred + err3 + fontend;
       return false;
     }
   }
 }
 if(a==0) {
     var pass = document.getElementById("pass").value;
     if(pass.length < 8) {
        document.getElementById("msg").innerHTML = fontred + err9 + fontend;
        return false;
     }
     for(i=0; i < pass.length; i++) {
       c = pass.charCodeAt(i);
       if(c == 0x20) {
         document.getElementById("msg").innerHTML = fontred + err8 + fontend;
         return false;
       }
       if(!((c >= 0x30 && c <= 0x39)||(c >= 0x41 && c <= 0x5A)||(c >= 0x61 && c <= 0x7A))) {
         document.getElementById("msg").innerHTML = fontred + err8 + fontend;
         return false;
       }
     }
 } else {
   if(a==1) {
     var pass = document.getElementById("pass").value;
     var pass2 = document.getElementById("pass2").value;
     if(pass.length!=pass2.length) {
        document.getElementById("msg").innerHTML = fontred + err7 + fontend;
        return false;
     }
     if(pass.length < 8) {
        document.getElementById("msg").innerHTML = fontred + err9 + fontend;
        return false;
     }
     for(i=0; i < pass.length; i++) {
       c = pass.charCodeAt(i);
       if(c == 0x20) {
         document.getElementById("msg").innerHTML = fontred + err8 + fontend;
         return false;
       }
       if(!((c >= 0x30 && c <= 0x39)||(c >= 0x41 && c <= 0x5A)||(c >= 0x61 && c <= 0x7A))) {
         document.getElementById("msg").innerHTML = fontred + err8 + fontend;
         return false;
       }
       if(c!=pass2.charCodeAt(i)) {
         document.getElementById("msg").innerHTML = fontred + err7 + fontend;
         return false;
       }
     }
     var email = document.getElementById("email").value;
     f = 0;
     for(i=0; i < email.length; i++) {
       c = email.charCodeAt(i);
       if(c == 0x40 && f == 0) {
          if(i==0) {
            f = -1;
          } else {
            f = 1;
          }
       }
       if(c == 0x2E && f == 1) {
          if(i >= email.length-2) {
            f = -1;
          } else {
            f = 2;
          }
       }
       if(!(c > 0x20 && c < 0x7F)) {
          f = -1;
       }
     }
     if(f!=2 || email.length<=5 || email.length>50) {
       document.getElementById("msg").innerHTML = fontred + err10 + fontend;
       return false;
     }
   }
   var magicfloat = 0.0042266845703125;
   var serial = document.getElementById("serial").value;
   if(serial.length!=15||serial.charCodeAt(2)!=0x2D||serial.charCodeAt(5)!=0x2D) {
     document.getElementById("msg").innerHTML = fontred + err4 + fontend;
     return false;
   }
   for(i=0; i < serial.length; i++) {
     c = serial.charCodeAt(i);
     if(i!=2 && i!=5) { if(!((c >= 0x30 && c <= 0x39)||(c >= 0x41 && c <= 0x46))) {
         document.getElementById("msg").innerHTML = fontred + err5 + fontend;
         return false;
     }}
   }
   var idfirst=serial.substr(0,2);var idsecond=serial.substr(3,2);var idhead=serial.substr(6,5);var idtail=serial.substr(11,4);var headi=parseInt(idhead);
   if(idfirst!=="52" || idsecond!=="81" || idhead==="00000" || idhead==="00036" || idhead!=headi || idtail!=="47E8") {
     document.getElementById("msg").innerHTML = fontred + err6 + fontend;
     return false;
   }
   if(headi>=200) {
     var d0=0x10000*1.0;var dd=0;var d1=headi/d0;for(i=0;i<d0;i++){dd=magicfloat*i-d1;var d2=dd;if(d2>-1){if(d2<0){d2=-d2;}d2=d2-parseInt(d2);if(d2<0.00001){headi=headi+parseInt(dd)*d0;break;}}}if(i>=d0){headi=0;}
   }
   if(headi==0) {
     document.getElementById("msg").innerHTML = fontred + err6 + fontend;
     return false;
   }
   document.getElementById("serial").value = headi;
 }
 return true;
}
