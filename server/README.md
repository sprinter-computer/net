# SprinterNet Gateway

Here you can find source codes for Sprinter Net server side software under Affero GPL.

## Install

To make it running on your server you need to create config.php like this:

    <?php
    $dbhost = '...'; // DB host (localhost)
    $dbname = '...'; // DB name
    $dbuser = '...'; // DB user
    $dbpass = '...'; // DB password
    $exsalt = '...'; // external salt (hexadecimal string)
    $insalt = '...'; // internal salt (ascii string)
    ?>

(put proper string values there to match your system).

Also you should create 3 tables in your MySQL/MariaDB:

    CREATE TABLE spr_action (
     id int unsigned primary key not null auto_increment,
     op char(8),
     port smallint,
     usr char(16) not null default "",
     adr char(16) not null default "127.0.0.1",
     utime int unsigned not null default 0,
     info varchar(100) not null default "",
     index spr_ac (op,usr,adr,utime)
    ) ENGINE=InnoDB;

    CREATE TABLE spr_user (
     id int unsigned primary key not null auto_increment,
     usr char(16) not null,
     pwd char(40) not null,
     email varchar(50) not null,
     utime int unsigned not null default 0,
     numc tinyint unsigned not null default 1,
     status tinyint unsigned not null default 1,
     unique index spr_uu (usr),
     unique index spr_ue (email),
     index spr_uc (usr,numc),
     index spr_ut (utime)
    ) ENGINE=InnoDB;

    CREATE TABLE spr_machine (
     id int unsigned primary key not null auto_increment,
     usr_id int unsigned not null,
     pin int unsigned not null,
     utime int unsigned not null default 0,
     etime int unsigned not null default 0,
     sig char(40),
     sid char(40),
     adr char(16) not null default "127.0.0.1",
     cnt int unsigned not null default 0,
     unique index spr_mp (pin),
     index spr_ms (usr_id,sid),
     index spr_mt (utime)
    ) ENGINE=InnoDB;

Main PHP script index.php is written to work by HTTPS (port 443) to be
accessible from PC and by HTTP (port 80 or other) to be accessible from Sprinter.
Currently it doesn't have a lot of functionality, but we are working on extention...

## Change Log

v1.0.1 (25-JUL-2021)

* extended database tables
* minimal password length reduced to 6 characters
* updated way to report errors in the table of actions
* added new operations: auth (creates sid), https (requires sid)
* added special cases for op=resolv (sprinternet and myself)

v1.0.0 (11-JUL-2021)

* initial version with registration and login page with counter
* supported operations: version, salt, time, resolv
