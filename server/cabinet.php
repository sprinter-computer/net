<?php
require('config.php'); // $dbhost, $dbname, $dbuser, $dbpass, $ausalt

$con = new mysqli($dbhost,$dbuser,$dbpass,$dbname);
if($con->connect_error) {
 echo "DB ERROR";
 exit;
}

$port = intval($_SERVER['SERVER_PORT']);
$addr = $_SERVER['REMOTE_ADDR'];
$unix = time();
$user = "";
$op = "";
$pin = 0;
$pwd = "";
$users = 0;
$ok = 0;

if(isset($_POST['user'])) {
  $user = preg_replace('/[^A-Za-z0-9\_]/','',$_POST['user']);
  $op = "cabinet";
}

echo "<HTML><HEAD><TITLE>SprinterNet Cabinet</TITLE>\n";
echo "<META HTTP-EQUIV=\"Content-Language\" content=\"en\">\n";
echo "<META HTTP-EQUIV=\"Content-Type\" content=\"text/html; charset=utf-8\">\n";
echo "</HEAD>\n";
echo "<BODY BGCOLOR=#FFFFFF>\n";

if(strcmp($op,"cabinet")==0) {

  if(isset($_POST['pin'])) {
     $pin = intval($_POST['pin']);
  }
  if(isset($_POST['pass'])) {
     $pwd = hash('ripemd160',preg_replace('/[^A-Za-z0-9]/','',$_POST['pass']));
  }

  if($pin<=0) {

    echo "INVALID PIN\n";

  } else {

    $unix5 = $unix - 300;
    $res = $con->query("select count(*) from (select usr from spr_action where usr!=\"\" and utime>=$unix5 group by usr) as users");
    if($res) {
       $rec = $res->fetch_row();
       $users = $rec[0];
       $res->close();
    }

    $username = "";
    $pwdhash = "";
    $email = "";
    $numc = "";

    $res = $con->query("select usr,pwd,email,numc from spr_user,spr_machine where spr_user.id=usr_id and pin=$pin");
    if($res) {
       $rec = $res->fetch_row();
       $username = $rec[0];
       $pwdhash = $rec[1];
       $email = $rec[2];
       $numc = $rec[3];
       $res->close();
    }

    if(strcmp($username,$user)!=0 || strcmp($pwdhash,$pwd)!=0) {

      echo "AUTHENTIFICATION FAILED\n";

    } else {

      $ok = 1;
      echo "Welcome, $user<br>\n";
      echo "Your E-mail is $email<br>\n";
      if($numc==1) {
        echo "You registered 1 computer<br>\n";
      } else {
        echo "You registered $numc computers<br>\n";
      }
      echo "<p>Active users: $users\n";
    }
  }

}

echo "</BODY></HTML>\n";

if(strcmp($op,"cabinet")==0 && $ok==0) {
  $user.="-".$pin;
}

$con->query("insert into spr_action (op,port,usr,adr,utime) values (\"$op\",$port,\"$user\",\"$addr\",$unix)");

$con->close();

?>
