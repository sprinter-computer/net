<?php
/*  sprinternet.io - SprinterNet Gateway by Shaos

    Copyright (C) 2021 Shaos <me@shaos.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

$systemname = "SprinterNet";
$version = "v1.0.1";
$hexversion = "010001";
$copyright = "2021 Shaos";
$contact = "admin@sprinternet.io";
$src = "https://gitlab.com/sprinter-computer/net";

require('config.php'); // $dbhost, $dbname, $dbuser, $dbpass, $exsalt, $insalt

$con = new mysqli($dbhost,$dbuser,$dbpass,$dbname);
if($con->connect_error) {
 echo "DB ERROR";
 exit;
}

$port = intval($_SERVER['SERVER_PORT']);
$addr = $_SERVER['REMOTE_ADDR'];
$unix = time();
$user = "";
$info = "";
$op = "login";
$lang = "ENG";
$counter = 0;
$registration = 0;
$pin = 0;
$pwd = "";
$pwdlen = 0;
$email = "";
$users = 0;

if(isset($_GET['user'])) {
  $user = preg_replace('/[^A-Za-z0-9\_]/','',$_GET['user']);
}
else if(isset($_POST['user'])) {
  $user = preg_replace('/[^A-Za-z0-9\_]/','',$_POST['user']);
  $op = "register";
  $registration = 1;
}

if(!isset($_GET['op'])) {

 echo "<HTML><HEAD><TITLE>$systemname</TITLE>\n";
 echo "<META HTTP-EQUIV=\"Content-Language\" content=\"en\">\n";
 echo "<META HTTP-EQUIV=\"Content-Type\" content=\"text/html; charset=utf-8\">\n";
 echo "</HEAD>\n";
 echo "<BODY BGCOLOR=#FFFFFF>\n";

 if($port==443) {

  $res = $con->query("select max(id) from spr_action");
  if($res) {
     $rec = $res->fetch_row();
     $counter = $rec[0];
     $res->close();
  }

  $unix5 = $unix - 300;
  $res = $con->query("select count(*) from (select usr from spr_action where usr!=\"\" and utime>=$unix5 group by usr) as users");
  if($res) {
     $rec = $res->fetch_row();
     $users = $rec[0];
     $res->close();
  }

  if(isset($_POST['serial'])) {
     $pin = intval($_POST['serial']);
  }
  if(isset($_POST['pass'])) {
     $pwd = hash('ripemd160',preg_replace('/[^A-Za-z0-9]/','',$_POST['pass']));
     $pwdlen = strlen($_POST['pass']);
  }
  if(isset($_POST['email'])) {
     if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
       $email = $_POST['email'];
       if($pwdlen < 6 || strlen($user) < 3) {
          $email = "INVALID";
       }
     } else {
       $email = "INVALID";
     }
  }
  if(isset($_POST['lang'])) {
     $lang = preg_replace('/[^A-Z]/','',$_POST['lang']);
  }

  echo "<center><table width=100% height=100% border=0>\n";
  echo "<tr><td colspan=2>&nbsp;</td></tr><tr>\n";
  echo "<td align=center colspan=2>\n";
  echo "<h2>$systemname Gateway</h2>\n";
  if($registration==1) {
    $result = 0;
    if(strcmp($email,"INVALID")!=0 &&
       $con->query("insert into spr_user (usr,pwd,email,utime) values (\"$user\",\"$pwd\",\"$email\",$unix)")) {
       $result = 1;
       $res = $con->query("select id from spr_user where usr=\"$user\"");
       if($res) {
          $result = 2;
          $rec = $res->fetch_row();
          $usr_id = $rec[0];
          $res->close();
          if($con->query("insert into spr_machine (usr_id,pin,utime,sig) values ($usr_id,$pin,$unix,\"$pwd\")")) {
            $result = 3;
          } else {
            $con->query("delete from spr_user where usr=\"$user\"");
          }
       }
    }
    if($result==3) {
      if(strcmp($lang,"RUS")==0) {
        echo "<h3><font color=#00C000>Пользователь ",$user," был зарегистрирован успешно! Запишите свой PIN: ",$pin,"</font></h3>\n";
      } else if(strcmp($lang,"ESP")==0) {
        echo "<h3><font color=#00C000>¡El usuario ",$user," se registró correctamente! Escriba su PIN: ",$pin,"</font></h3>\n";
      } else {
        echo "<h3><font color=#00C000>User ",$user," was registered successfully! Write your PIN down: ",$pin,"</font></h3>\n";
      }
      $registration = 2;
    } else {
      if(strcmp($lang,"RUS")==0) {
        echo "<h3><font color=#C00000>Регистрация пользователя ",$user," &lt;",$email,"&gt; НЕ УДАЛАСЬ (",$result,")</font></h3>\n";
      } else if(strcmp($lang,"ESP")==0) {
        echo "<h3><font color=#C00000>Registro del usuario ",$user," &lt;",$email,"&gt FALLIDO (",$result,")</font></h3>\n";
      } else {
        echo "<h3><font color=#C00000>Registration of user ",$user," &lt;",$email,"&gt FAILED (",$result,")</font></h3>\n";
      }
    }
  }
  echo "<form method=\"post\" onsubmit=\"return validate(0)\" action=\"cabinet.php\">\n";
  echo "Username:<br>\n";
  echo "<input name=\"user\" ";
  if($registration==2) {
    echo "value=",$user;
  }
  echo " type=\"text\" size=16 maxlength=16><br>\n";
  echo "Password:<br>\n";
  echo "<input name=\"pass\" type=\"password\" size=16><br>\n";
  echo "PIN:<br>\n";
  echo "<input name=\"pin\" ";
  if($registration==2) {
    echo "value=",$pin;
  }
  echo " type=\"text\" size=8><br><br>\n";
  echo "<input name=\"submit\" type=\"submit\" value=\"Login\">\n";
  echo "<input name=\"reset\" type=\"reset\" value=\"Cancel\">\n";
  echo "</form>\n";
  echo "<p>\n";
  echo "<a href=\"faq-eng.html\">F.A.Q.</a> | \n";
  echo "<a href=\"reg-eng.html\">Registration</a> | \n";
  echo "<a href=\"pin-eng.html\">Forgot my PIN</a> | \n";
  echo "<a href=\"mailto:$contact\">Contact Us</a> |\n";
  echo "<a href=\"$src\">Source Code</a></font>\n";
  echo "</p>\n";
  echo "<p><font size=\"-1\">&copy; $copyright</font></p>\n";
  echo "<p>";
  $digits = intval(log10($counter));
  while($digits >= 0) {
    $tens = pow(10,$digits);
    $digit = intval($counter/$tens);
    $counter = $counter - $digit*$tens;
    echo "<img src=\"digit",$digit,".gif\">";
    $digits = $digits - 1;
  }
  echo "</p>\n";
  echo "</td>\n";
  echo "</tr><tr>\n";
  echo "<td valign=bottom align=left id=\"active\"><font size=\"-1\">Active users: $users</font></td>\n";
  echo "<td valign=bottom align=right><font size=\"-1\">$systemname $version</font></td>\n";
  echo "</tr>\n";
  echo "</table></center>\n";

 } else {

  $op = "links";
  echo "<h2>$systemname Links</h2>\n";
  echo "<ul>\n";
  echo "<li><a href=\"http://www.nedopc.org/nedopc/Sprinter/About\">Sprinter Unofficial</a>\n";
  echo "<li><a href=\"http://www.nedopc.org/forum/\">Forum nedoPC</a>\n";
  echo "</ul>\n";

 }

 echo "</BODY></HTML>\n";

} else {

 $op = preg_replace('/[^a-z]/', '',$_GET['op']);

 if($port!=443) {

    $param = "";
    if(isset($_GET['param'])) {
      $param = urldecode($_GET['param']);
    }
    $host = "sprinternet.io";
    if(isset($_GET['host'])) {
      $host = preg_replace('/[^A-Za-z0-9\-\.]/','',$_GET['host']);
    }
    $usid = "";
    if(isset($_GET['sid'])) {
      $usid = preg_replace('/[^a-z0-9]/','',$_GET['sid']);
    }

    if(strcmp($op,"version")==0) {

      echo $hexversion;

    } else if(strcmp($op,"salt")==0) {

      echo $exsalt;

    } else if(strcmp($op,"time")==0) {

      if(!empty($param)) {
         $time = strtotime($param);
      } else {
         $time = $unix;
      }
      echo dechex($time),gmdate("YmdHis",$time);

    } else if(strcmp($op,"resolv")==0) {

      if(strcmp($host,"sprinternet")==0) {
        $ip = explode(".",gethostbyname("sprinternet.io"));
      } else if(strcmp($host,"myself")==0) {
        $ip = explode(".",$addr);
      } else {
        $ip = explode(".",gethostbyname($host));
      }
      $ip1 = dechex(intval($ip[0]));
      $ip2 = dechex(intval($ip[1]));
      $ip3 = dechex(intval($ip[2]));
      $ip4 = dechex(intval($ip[3]));
      if(strlen($ip1)==1) {
       $ip1 = '0'.$ip1;
      }
      if(strlen($ip2)==1) {
       $ip2 = '0'.$ip2;
      }
      if(strlen($ip3)==1) {
       $ip3 = '0'.$ip3;
      }
      if(strlen($ip4)==1) {
       $ip4 = '0'.$ip4;
      }
      $info = "$host $ip[0].$ip[1].$ip[2].$ip[3]";
      echo $ip1,$ip2,$ip3,$ip4;

    } else if(strcmp($op,"auth")==0) {

      $res = $con->query("select id,pwd from spr_user where usr=\"$user\"");
      if($res) {
         $rec = $res->fetch_row();
         if($rec) {
           $usr_id = $rec[0];
           $usr_pw = $rec[1];
           $res->close();
           $ok = 0;
           $res = $con->query("select id,pin,sid,cnt from spr_machine where usr_id=$usr_id");
           if($res) {
              $rec = $res->fetch_row();
              if($rec && !$ok) { // TODO: while for multiple machines
                 $pc_id = $rec[0];
                 $pc_pin = sprintf("%08x",intval($rec[1]));
                 $pc_sid = $rec[2];
                 $pc_cnt = intval($rec[3])+1;
                 $etime = intval($unix)+24*3600-60; // 23h59m
                 $gmt = gmdate("Ymd",$unix);
                 $tmp = "$usr_pw$gmt$pc_pin$exsalt";
                 $hash = hash('ripemd160',$tmp);
                 if(strcmp($hash,$param)==0) {
                   $ok = 1;
                   $tmp2 = "$pc_pin$gmt$insalt$addr";
                   $sid = hash('ripemd160',$tmp2);
                   if(strcmp($sid,$pc_sid)!=0) {
                     $res->close();
                     $res = $con->query("update spr_machine set sid=\"$sid\",etime=$etime,cnt=$pc_cnt,adr=\"$addr\" where id=$pc_id");
                     if($res) {
                        $ok = 2;
                     }
                   }
                   $info = $sid;
                   echo "$sid";
                 }
//                 echo "DEBUG $op $addr $user $usr_id $tmp $hash $tmp2 $sid $pc_cnt $unix $etime $ok!!!";
              }
           }
           if(!$ok) {
              echo "02";
              $info = "ERROR-2";
           }
         } else {
           echo "02";
           $info = "ERROR-2U";
         }
         $res->close();
      }
    } else if(strcmp($op,"https")==0) {

      if(intval(gethostbyname($host))==0) {
         echo "00";
         $info = "$host ERROR";
      } else {
       $res = $con->query("select id from spr_user where usr=\"$user\"");
       if($res) {
         $rec = $res->fetch_row();
         if($rec) {
           $usr_id = $rec[0];
           $res->close();
           $ok = 0;
           $res = $con->query("select sid,etime,adr from spr_machine where usr_id=$usr_id");
           if($res) {
              $rec = $res->fetch_row();
              if($rec && !$ok) { // TODO: while for multiple machines
                 $pc_sid = $rec[0];
                 $pc_tim = $rec[1];
                 $pc_adr = $rec[2];
                 if(strcmp($pc_sid,$usid)==0) {
                    if(intval($unix) > intval($pc_tim)) {
                      echo "03";
                      $info = "$host ERROR-3T";
                    } else {
                      if(strcmp($pc_adr,$addr)!=0) {
                        echo "03";
                        $info = "$host ERROR-3A";
                      } else {
                        $ok = 1;
                        $info = $host;
                        echo file_get_contents("https://".$host."/".$param);
                      }
                    }
                 }
              }
           }
           if(!$ok) {
              echo "03";
              $info = "$host ERROR-3";
           }
         } else {
           echo "03";
           $info = "$host ERROR-3U";
         }
         $res->close();
       }
      }

    } else {
      echo "01";
      $info = "ERROR-1";
    }

 } else {
   echo "ERROR"; // port 443 with defined op
   $user = "ERROR";
   $info = "ERROR";
 }

}

if($registration==1) {
  $con->query("insert into spr_action (op,port,usr,adr,utime,info) values (\"$op\",$port,\"$user\",\"$addr\",$unix,\"ERROR-$pin\")");
} else if($info) {
  $con->query("insert into spr_action (op,port,usr,adr,utime,info) values (\"$op\",$port,\"$user\",\"$addr\",$unix,\"$info\")");
} else {
  $con->query("insert into spr_action (op,port,usr,adr,utime) values (\"$op\",$port,\"$user\",\"$addr\",$unix)");
}

$con->close();

?>
