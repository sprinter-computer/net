/* GigasRGBxMaker.c - Alexander Shabarshin (April 2021)

   How to build:
                 gcc GigasRGBxMaker.c -o GigasRGBxMaker -lm

   ATTENTION: THIS CODE IS PUBLIC DOMAIN
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

unsigned char pal[256][3];

unsigned char needcolor[256][3];
int needcolorn = 0;

/*

Color number    Binary value    BRIGHT 0 (RGB)   BRIGHT 1 (RGB)  Color name
---------------------------------------------------------------------------
      0             000         #000000          #000000         black
      1             001         #0000CD          #0000FF         blue
      2             010         #CD0000          #FF0000         red
      3             011         #CD00CD          #FF00FF         magenta
      4             100         #00CD00          #00FF00         green
      5             101         #00CDCD          #00FFFF         cyan
      6             110         #CDCD00          #FFFF00         yellow
      7             111         #CDCDCD          #FFFFFF         white
*/

/* sRGB scale (gamma=2.2):   0%    ----  20%   40%   60%   80%   100% */
   unsigned char scale[] = { 0x00, 0x00, 0x7C, 0xAA, 0xCB, 0xE7, 0xFF };
/*                           0%          48.6% 66.6% 79.6% 90.6% 100% */

void setpal(void)
{
   unsigned char *po; /* address to apply palette */
   int i,x,y,c1,c2,n=0;
   for(i=0;i<256;i++)
   {
      po = (unsigned char*)&pal[i];
      /* palette_index = (first_color<<4)|second_color */
      c1 = (i&0x80)?3:2;
      c2 = (i&0x08)?3:2;
      x = i&15;
      y = i>>4;
      if(y < x)
      {
         if(y==0) po[0] = po[1] = po[2] = x<<3;
         else if(y==1) po[0] = po[1] = po[2] = (x+16)<<3;
         else if(y==11)
         {
            if(x==12) po[0] = po[1] = po[2] = 0x80;
            if(x==13) po[0] = po[1] = po[2] = 0x88;
            if(x==14){po[0]=0x95;po[1]=0x7A;po[2]=0x10;} /* ZXart dark yellow */
            if(x==15){po[0]=0xE5;po[1]=0xBC;po[2]=0x19;} /* ZXart yellow */
         }
         else if(y==12)
         {
            if(x==13){po[0]='v';po[1]='1';po[2]='0';} /* ZX-palette v1.0 */
            if(x==14){po[0]=0x95;po[1]=0x10;po[2]=0x10;} /* ZXart dark red */
            if(x==15){po[0]=0xE5;po[1]=0x19;po[2]=0x19;} /* ZXart red */
         }
         else if(y==13)
         {
            if(x==14){po[0]=0x24;po[1]=0x5A;po[2]=0x0A;} /* ZXart dark green */
            if(x==15){po[0]=0x38;po[1]=0x8A;po[2]=0x0F;} /* ZXart green */
         }
         else if(y==14)
         {
            if(x==15){po[0]=0x15;po[1]=0x3B;po[2]=0xBD;} /* ZXart blue */
         }
         else /* y=2..10 */
         {
            if(n<needcolorn)
            {
               po[0] = needcolor[n][0];
               po[1] = needcolor[n][1];
               po[2] = needcolor[n][2];
               n++;
            }
            else if(y==10)
            {
               if(x==14){po[0]=0x15;po[1]=0xBC;po[2]=0xBD;} /* ZXart compatible cyan */
               if(x==15){po[0]=0xE5;po[1]=0x19;po[2]=0xBD;} /* ZXart compatible magenta */
            }
         }
         continue;
      }
      po[2] = scale[((i&0x10)?c1:0)+((i&0x01)?c2:0)];
      po[0] = scale[((i&0x20)?c1:0)+((i&0x02)?c2:0)];
      po[1] = scale[((i&0x40)?c1:0)+((i&0x04)?c2:0)];
   }
}

int closest(int r, int g, int b, int *err)
{
 int i,j,rr,gg,bb;
 *err = 0x7FFFFFFF;
 for(j=0;j<256;j++)
 {
    rr = pal[j][0] - r;
    gg = pal[j][1] - g;
    bb = pal[j][2] - b;
#if 0
    if(rr<0) rr=-rr;
    if(gg<0) gg=-gg;
    if(bb<0) bb=-bb;
    if(bb > rr) rr = bb;
    if(gg > rr) rr = gg;
#else
    rr = rr*rr + gg*gg + bb*bb;
#endif
    if(rr < *err)
    {
       i = j;
       *err = rr;
    }
 }
 *err = (int)sqrt((double)*err);
 return i;
}

int main(int argc, char** argv)
{
 int i,r,g,b,k,kk=0,add;
 FILE *f;
 memset(pal,0,sizeof(pal));
 setpal();
 for(r=0;r<=5;r++)
 {
   k = 0;
   printf("\nR=%i (#%02X)\nG\\ 0 1 2 3 4 5 <-- B\n",r,scale[r+1]);
   for(g=0;g<=5;g++)
   {
     printf("%2d",g);
     for(b=0;b<=5;b++)
     {
        for(i=0;i<256;i++)
        {
            if(pal[i][0]!=scale[r+1]) continue;
            if(pal[i][1]!=scale[g+1]) continue;
            if(pal[i][2]!=scale[b+1]) continue;
            break;
        }
        if(i<256)
        {
            k++;
            printf(" @");
        }
        else
        {
            needcolor[needcolorn][0] = scale[r+1];
            needcolor[needcolorn][1] = scale[g+1];
            needcolor[needcolorn][2] = scale[b+1];
            add = 1;
            if(r==5 && g!=1 && b!=1) add = 0;
            if(g==5 && r!=1 && b!=1) add = 0;
            if(b==5 && r!=1 && g!=1) add = 0;
            if(!add && (r==g || g==b || b==r) && (r!=4 && g!=4 && b!=4)) add = 1;
            if(add)
            {
                needcolorn++;
                printf(" .");
            }
            else printf("  ");
        }
     }
     printf("|\n");
   }
   printf("---------------\ntotal=%i\n",k);
   kk += k;
 }
 printf("\nTotal colors: %i\n",kk);
 printf("Needed colors: %i\n\n",needcolorn);
 setpal();
 f = fopen("GigasRGBx.gpl","wt");
 if(f==NULL) return -1;
 fprintf(f,"GIMP Palette\n");
 fprintf(f,"Name: GigasRGBx\n");
 fprintf(f,"#\n");
 for(i=0;i<256;i++)
 {
//   printf("%03d) #%2.2X%2.2X%2.2X\n",i,pal[i][0],pal[i][1],pal[i][2]);
   fprintf(f,"%3d %3d %3d #%2.2X%2.2X%2.2X [%i]\n",pal[i][0],pal[i][1],pal[i][2],pal[i][0],pal[i][1],pal[i][2],i);
 }
 fclose(f);

 i = closest(0x00,0x00,0x00,&k);
 printf("EGA black is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0x00,0x00,0xAA,&k);
 printf("EGA blue is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0x00,0xAA,0x00,&k);
 printf("EGA green is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0x00,0xAA,0xAA,&k);
 printf("EGA cyan is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0xAA,0x00,0x00,&k);
 printf("EGA red is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0xAA,0x00,0xAA,&k);
 printf("EGA magenta is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0xAA,0x55,0x00,&k);
 printf("EGA brown is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0xAA,0xAA,0xAA,&k);
 printf("EGA white is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0x55,0x55,0x55,&k);
 printf("EGA gray is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0x55,0x55,0xFF,&k);
 printf("EGA bright blue is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0x55,0xFF,0x55,&k);
 printf("EGA bright green is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0x55,0xFF,0xFF,&k);
 printf("EGA bright cyan is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0xFF,0x55,0x55,&k);
 printf("EGA bright red is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0xFF,0x55,0xFF,&k);
 printf("EGA bright magenta is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0xFF,0xFF,0x55,&k);
 printf("EGA bright yellow is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);
 i = closest(0xFF,0xFF,0xFF,&k);
 printf("EGA bright white is %i (err=%2.2f%%)",i,k/2.55);
 printf("\t#%02X%02X%02X\n",pal[i][0],pal[i][1],pal[i][2]);

 return 0;
}
