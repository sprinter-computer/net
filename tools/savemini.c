/* savemini.c - Alexander Shabarshin (Oct 2021)

   Miniature creation testing for SXG and SCR images

   How to build:
                 gcc savemini.c -o savemini -lpng

   ATTENTION: THIS CODE IS PUBLIC DOMAIN
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <png.h>

//#define DEBUG

int savepng(char* filename, int width, int height, unsigned char* data)
{
   /* Assume that it's RGB888 image */
   int bitdepth = 8;
   int colortype = PNG_COLOR_TYPE_RGB;
   int transform = PNG_TRANSFORM_IDENTITY;
   int i = 0;
   int r = 0;
   FILE* fp = NULL;
   png_structp png_ptr = NULL;
   png_infop info_ptr = NULL;
   static png_bytep row_pointers[1024];
   if(NULL == data) { r = -1; goto endlabel; }
   if(!filename || !filename[0]) { r = -2; goto endlabel; }
   if(NULL == (fp = fopen(filename, "wb"))) { r = -4; goto endlabel; }
   if(NULL == (png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL))) { r = -5; goto endlabel; }
   if(NULL == (info_ptr = png_create_info_struct(png_ptr))) { r = -6; goto endlabel; }
   png_set_IHDR(png_ptr, info_ptr, width, height, bitdepth,
                colortype,          /* PNG_COLOR_TYPE_{GRAY, PALETTE, RGB, RGB_ALPHA, GRAY_ALPHA, RGBA, GA} */
                PNG_INTERLACE_NONE, /* PNG_INTERLACE_{NONE, ADAM7 } */
                PNG_COMPRESSION_TYPE_BASE,
                PNG_FILTER_TYPE_BASE);
   for(i = 0; i < height; ++i) row_pointers[i] = data + i*width*3;
   png_init_io(png_ptr, fp);
   png_set_rows(png_ptr, info_ptr, row_pointers);
   png_write_png(png_ptr, info_ptr, transform, NULL);
 endlabel:
   if(NULL != fp)
   {
     fclose(fp);
     fp = NULL;
   }
   if(NULL != png_ptr)
   {
     if(NULL == info_ptr) r = -7;
     png_destroy_write_struct(&png_ptr, &info_ptr);
   }
   return r;
}

#include "gigasrgb.c"

unsigned char colormap[1024];
unsigned short colorname[1024];
short pal[256];

void docolormap(void)
{
  int r,g,b,i,j,k,l,m,e,h;
  int rr,gg,bb;
  int rgb555;
  for(i=0;i<sizeof(colormap);i++) colormap[i] = 173;
  for(r=0;r<32;r+=4){
  for(g=0;g<32;g+=2){
  for(b=0;b<32;b+=4){
    rgb555 = (r<<10)|(g<<5)|b;
    l = rgb555 & 255;
    h = rgb555 >> 8;
    i = (l & 0x8C)|(h & 0x73);
    if(l & 0x10) i += 256;
    if(l & 0x40) i += 512; /* more green */
#if 1
    if(colormap[i]!=173)
         printf("\nERROR: index %i was already used!\n\n",i);
//    else printf("RGB %4.4X -> %i\n",rgb555,i);
#endif
    e = 1000000;
    m = -1;
    for(j=0;j<256;j++)
    {
#if 0
       if(j > 1 && j <= 15) continue;
       if(j > 18 && j <= 31) continue;
#endif
       k = j+j+j;
       rr = (r<<3)-rgb[k];
       gg = (g<<3)-rgb[k+1];
       bb = (b<<3)-rgb[k+2];
       k = rr*rr + gg*gg + bb*bb;
       if(k < e)
       {
          e = k;
          m = j;
       }
    }
    colormap[i] = m;
#ifdef DEBUG
    printf("RGB %4.4X -> [%04d]=%i\terr=%i\n",rgb555,i,m,e);
#endif
    colorname[i] = rgb555;
  }}}
#if 1
  printf("\nCOLOTRANS:\n");
  for(i=0;i<sizeof(colormap);i++)
  {
     rgb555 = colorname[i];
     k = colormap[i]*3;
     printf("\tDB\t%i\t; [%03d] #%4.4X (#%2.2X%2.2X%2.2X -> #%2.2X%2.2X%2.2X)\n",colormap[i],i,
            rgb555,(rgb555>>10)<<3,((rgb555>>5)&0x1F)<<3,(rgb555&0x1F)<<3,rgb[k],rgb[k+1],rgb[k+2]);
  }
#endif
}

int onescounts[256];

void fillones(void)
{
  int h,l,i,c[16]={0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4};
  for(i=0;i<256;i++)
  {
     h = c[i>>4];
     l = c[i&15];
     onescounts[i] = (h<<4)|l;
  }
#if 1
  printf("\nONECOUNTS:\n");
  for(i=0;i<256;i++)
  {
    printf("\tDB\t%i\t; [#%2.2X]\n",onescounts[i],i);
  }
#endif
}

#define SCRTH1 4
#define SCRTH2 11

#define SXGW 256
#define SXGH 192

int main(int argc, char **argv)
{
   int i,j,k,a,b,c,c1,c2,hi,lo,dx,dy,bb,tt,t=0;
   unsigned char counters[32][8],attr[32][24];
   int w = 64;
   int h = 48;
   int offx,offy,step=SXGW/w;
   FILE *f;
   unsigned char* img;
   char filename[256],*po;

   if(argc<2)
   {
       printf("\nUsage\n\tsavemini filename\n\n");
       return -11;
   }
   strcpy(filename,argv[1]);
   po = strrchr(filename,'.');
   if(po!=NULL)
   {
     if(!strcmp(po,".sxg")) t=2;
     if(!strcmp(po,".SXG")) t=2;
     if(!strcmp(po,".scr")) t=1;
     if(!strcmp(po,".SCR")) t=1;
   }
   if(!t)
   {
     printf("\nERROR: Wrong file type %s\n\n",filename);
     return -12;
   }
   f = fopen(filename,"rb");
   if(f==NULL)
   {
     printf("\nERROR: Cant open file %s\n\n",filename);
     return -13;
   }
   img = (unsigned char*)malloc(w*h*3);
   if(img==NULL){fclose(f);return -10;}

   if(t==2)
   {
      docolormap();
/*
+#0000 #04 #7f+"SXG" - 4 байта сигнатура, что это формат файла SXG
+#0004 #01 1 байт версия формата
+#0005 #01 1 байт цвет фона (используется для очистки)
+#0006 #01 1 байт тип упаковки данных (#00 - данные не пакованы)
+#0007 #01 1 байт формат изображения (1 - 16ц, 2 - 256ц)
+#0008 #02 2 байта ширина изображения
+#000a #02 2 байта высота изображения

(далее указываются смещения, для того, что бы можно было расширить заголовок)
+#000c #02 смещение от текущего адреса до начала данных палитры
+#000e #02 смещение от текущего адреса до начала данных битмап

Собственно начало данных палитры
+#0010 #0200 512 байт палитра

и начало данных битмап
+#0210 #xxxx данные битмап

xRRrrrGG gggBBbbb
*/
     k = 0;
     if(fgetc(f)!=0x7F) k++;
     if(fgetc(f)!='S') k++;
     if(fgetc(f)!='X') k++;
     if(fgetc(f)!='G') k++;
     if(k)
     {
       printf("\nERROR: File %s is not SXG\n\n",filename);
       fclose(f);
       free(img);
       return -14;
     }
     else
     {
        b = fgetc(f);
        fgetc(f);fgetc(f);
        tt = fgetc(f);
        if(tt!=1 && tt!=2)
        {
           printf("\nERROR: File %s is not 16-color or 256-color SXG\n\n",filename);
           fclose(f);
           free(img);
           return -15;
        }
        dx = fgetc(f);
        dx |= fgetc(f)<<8;
        dy = fgetc(f);
        dy |= fgetc(f)<<8;
        printf("\nSGX v%i %ix%i (%i bits per pixel)\n",b,dx,dy,(tt==2)?8:4);
        if(dx < SXGW || dy < SXGH)
        {
           printf("\nERROR: SXG File %s is too small\n\n",filename);
           fclose(f);
           free(img);
           return -16;
        }
        offx = (dx-SXGW)>>1;
        offy = (dy-SXGH)>>1;
        k = fgetc(f);
        k |= fgetc(f)<<8;
        k -= 2;
        b = fgetc(f);
        b |= fgetc(f)<<8;
        b >>= 1;
        printf("\nskip %i bytes and read %i color palette\n",k,b);
        for(i=0;i<k;i++) fgetc(f); /* in case of large header */
        for(i=0;i<b;i++)
        {
          lo = fgetc(f);
          hi = fgetc(f);
          k = (lo & 0x8C)|(hi & 0x73);
          if(lo & 0x10) k += 256;
          if(lo & 0x40) k += 512;
          pal[i] = k;
#ifdef DEBUG
          j = colormap[k]*3;
          printf("[%i] %2.2X%2.2X -> %i (%2.2X%2.2X%2.2X)\n",i,hi,lo,k,rgb[j],rgb[j+1],rgb[j+2]);
#endif
        }
        k = 0;
        for(j=0;j<dy;j++)
        {
          for(i=0;i<dx;i++)
          {
             if(tt==2)
             {
                lo = fgetc(f);
             }
             else
             {
                if(!(i&1))
                {
                   bb = fgetc(f);
                   lo = bb >> 4;
                }
                else
                {
                   lo = bb & 15;
                }
             }
             if(i>=offx && i<offx+SXGW && j>=offy && j<offy+SXGH && ((i-offx)%step)==0 && ((j-offy)%step)==0)
             {
               c = pal[lo];
               b = colormap[c];
#ifdef DEBUG
               printf("[%i:%i] %i -> %i -> %i (%2.2X%2.2X%2.2X)\n",i,j,lo,c,b,rgb[b*3],rgb[b*3+1],rgb[b*3+2]);
#endif
               b = b + b + b;
               img[k++] = rgb[b];
               img[k++] = rgb[b+1];
               img[k++] = rgb[b+2];
             }
          }
        }
     }
   }

   if(t==1)
   {
     fillones();
     fseek(f,6144,SEEK_SET);
     for(j=0;j<24;j++)
     {
       for(i=0;i<32;i++)
       {
          attr[i][j] = fgetc(f);
       }
     }
     fseek(f,0,SEEK_SET);
     for(k=0;k<3;k++)
     {
       for(j=0;j<8;j++)
       {
         for(i=0;i<32;i++) counters[i][j] = onescounts[fgetc(f)];
       }
       for(j=0;j<8;j++)
       {
         for(i=0;i<32;i++) counters[i][j] += onescounts[fgetc(f)];
       }
       for(j=0;j<8;j++)
       {
         for(i=0;i<32;i++) counters[i][j] += onescounts[fgetc(f)];
       }
       for(j=0;j<8;j++)
       {
         for(i=0;i<32;i++)
         {
            a = attr[i][j+(k<<3)];
            b = onescounts[fgetc(f)];
            bb = (i<<1)+((j+(k<<3))<<7);
            hi = (counters[i][j]>>4) + (b>>4);
            lo = (counters[i][j]&15) + (b&15);
            if(hi <= SCRTH1)
            {
                c = (a>>3)&7;
                if(a&0x40) c+=8;
                c |= c<<4;
            }
            else if(hi <= SCRTH2)
            {
                c1 = a&7;
                c2 = (a>>3)&7;
                if(a&0x40){c1+=8;c2+=8;}
                if(c1 > c2)
                   c = (c1<<4)|c2;
                else
                   c = (c2<<4)|c1;
            }
            else
            {
                c = a&7;
                if(a&0x40) c+=8;
                c |= c<<4;
            }
            c *= 3;
            img[3*bb+0] = rgb[c];
            img[3*bb+1] = rgb[c+1];
            img[3*bb+2] = rgb[c+2];
            if(lo <= SCRTH1)
            {
                c = (a>>3)&7;
                if(a&0x40) c+=8;
                c |= c<<4;
            }
            else if(lo <= SCRTH2)
            {
                c1 = a&7;
                c2 = (a>>3)&7;
                if(a&0x40){c1+=8;c2+=8;}
                if(c1 > c2)
                   c = (c1<<4)|c2;
                else
                   c = (c2<<4)|c1;
            }
            else
            {
                c = a&7;
                if(a&0x40) c+=8;
                c |= c<<4;
            }
            c *= 3;
            img[3*bb+3] = rgb[c];
            img[3*bb+4] = rgb[c+1];
            img[3*bb+5] = rgb[c+2];
         }
       }

       for(j=0;j<8;j++)
       {
         for(i=0;i<32;i++) counters[i][j] = onescounts[fgetc(f)];
       }
       for(j=0;j<8;j++)
       {
         for(i=0;i<32;i++) counters[i][j] += onescounts[fgetc(f)];
       }
       for(j=0;j<8;j++)
       {
         for(i=0;i<32;i++) counters[i][j] += onescounts[fgetc(f)];
       }
       for(j=0;j<8;j++)
       {
         for(i=0;i<32;i++)
         {
            a = attr[i][j+(k<<3)];
            b = onescounts[fgetc(f)];
            bb = (i<<1)+((j+(k<<3))<<7)+64;
            hi = (counters[i][j]>>4) + (b>>4);
            lo = (counters[i][j]&15) + (b&15);
            if(hi <= 4)
            {
                c = (a>>3)&7;
                if(a&0x40) c+=8;
                c |= c<<4;
            }
            else if(hi <= 11)
            {
                c1 = a&7;
                c2 = (a>>3)&7;
                if(a&0x40){c1+=8;c2+=8;}
                if(c1 > c2)
                   c = (c1<<4)|c2;
                else
                   c = (c2<<4)|c1;
            }
            else
            {
                c = a&7;
                if(a&0x40) c+=8;
                c |= c<<4;
            }
            c *= 3;
            img[3*bb+0] = rgb[c];
            img[3*bb+1] = rgb[c+1];
            img[3*bb+2] = rgb[c+2];
            if(lo <= 5)
            {
                c = (a>>3)&7;
                if(a&0x40) c+=8;
                c |= c<<4;
            }
            else if(lo <= 10)
            {
                c1 = a&7;
                c2 = (a>>3)&7;
                if(a&0x40){c1+=8;c2+=8;}
                if(c1 > c2)
                   c = (c1<<4)|c2;
                else
                   c = (c2<<4)|c1;
            }
            else
            {
                c = a&7;
                if(a&0x40) c+=8;
                c |= c<<4;
            }
            c *= 3;
            img[3*bb+3] = rgb[c];
            img[3*bb+4] = rgb[c+1];
            img[3*bb+5] = rgb[c+2];
         }
       }


     }
   }

   fclose(f);

   k = savepng("savemini.png",w,h,img);

   printf("\nsavepng %i\n\n",k);

   free(img);

   return 0;
}

