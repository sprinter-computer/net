; INET2.ASM by Shaos 25-JUL-2021 (PUBLIC DOMAIN)
; SprinterNet repo: https://gitlab.com/sprinter-computer/net
; Hexadecimal print was taken from ISATEST.ASM (January 2021)
; Decimal print was taken from ShaOS (1998) with modification from nedoPC SDK (2002)

	org   #7F00

;EXE-file header:
        dw 5845h     ; EXE Signature
        db 45h       ; Reserved (EXE type)
        db 00h       ; Version of EXE file
        dw 0200h     ; Code offset
        dw 0000h
        dw 0000h     ; Primary loader size or 0 (no primary loader)
        dw 0000h     ; Reserved
        dw 0000h     ; Reserved
        dw 0000h     ; Reserved
        dw START     ; Loading address
        dw START     ; Starting address (register PC)
        dw ENDSP     ; Stack address (register SP)
        ds 490       ; Reserved

PAGE0	EQU	082h
PAGE1	EQU	0A2h
PAGE2	EQU	0C2h
PAGE3	EQU	0E2h

;code of program

START:

; Write welcome message

        ld   hl,msg
        ld   c,5ch
        rst   10h

; Save old identifiers of pages in windows 1 and 3

	in	a,(PAGE1)
	ld	(pg1),a
	in	a,(PAGE3)
	ld	(pg3),a

	ld	bc,023Dh ; allocate 2 pages
	rst	10h
	jp	c,exit
	ld	(mem),a
	ld	bc,003Bh ; index 0 to page 3
	rst	10h
	jp	c,lend
	ld	a,(mem)
	ld	bc,0139h ; index 1 to page 1
	rst	10h
	jp	c,lend

	ld	hl,buf
	ld	bc,#00D0 ; netinit
	rst	10h ; HL saved !!!
	jp	c,lend
	push	af
	push	bc
	ld	c,5Ch ; PCHARS
	rst	10h
	call	print_newline
	ld	hl,ver
	ld	c,5Ch ; PCHARS
	rst	10h
	pop	bc
	pop	af
	push	bc
	ld	c,a
	ld	b,0
	call	print_dec
	call	print_dot
	pop	bc
	push	bc
	ld	c,b
	ld	b,0
	call	print_dec
	call	print_dot
	pop	bc
	ld	b,0
	call	print_dec
	call	print_newline

	ld	hl,get
	ld	c,5Ch ; PCHARS
	rst	10h
	call	print_newline

	ld	iy,url
	ld	hl,ENDSP
	ld	de,10000
	ld	bc,#30D0 ; httpget
	rst	10h
	jp	c,lerr

	ld	hl,ENDSP
	add	hl,bc
	ld	(hl),0

	push	bc
	ld	hl,msg2
	ld	c,5Ch
	rst	10h
	pop	bc
	call	print_dec
	call	print_newline

	ld	hl,ENDSP
loop:
	ld	a,(hl)
	push	hl
	cp	0
	jr	z,loop0
	cp	10
	jr	nz,loop1
	ld	a,13
	call	print_char
	ld	a,10
loop1:	call	print_char
	pop	hl
	inc	hl
	jr	loop
loop0:
	call	print_newline
	ld	hl,msg3
	ld	c,5Ch ; PCHARS
	rst	10h
	ld	hl,ENDSP
	ld	de,buf4
	call	dehex
	ld	b,0
	ld	a,(buf4)
	ld	c,a
	call	print_dec
	call	print_dot
	ld	b,0
	ld	a,(buf4+1)
	ld	c,a
	call	print_dec
	call	print_dot
	ld	b,0
	ld	a,(buf4+2)
	ld	c,a
	call	print_dec
	call	print_dot
	ld	b,0
	ld	a,(buf4+3)
	ld	c,a
	call	print_dec
	call	print_newline

; Wait keypress

lwait:
        ld   c,30h
        rst   10h

lend:
	ld	a,(pg1)
	out	(PAGE1),a
	ld	a,(pg3)
	out	(PAGE3),a
	ld	a,(mem)
	ld	c,3Eh ; FREEMEM
	rst	10h

; Exit program

exit:
        ld   bc,0041h
        rst   10h

; Error BC

lerr:
	push	bc
	ld	a,b
	call	print_hex
	pop	bc
	ld	a,c
	call	print_hex
	ld	hl,err
	ld	c,5Ch ; PCHARS
	rst	10h
	jr	lwait

; Convert hexadecimal string to sequence of bytes
; HL - pointer to hexadecimal string
; DE - pointer to buffer to store bytes
; Output:
; stored HL and BC
; shifted DE

dehex:	push	hl
	push	bc
dehex0:	ld	a,(hl)
	cp	0
	jr	z,dehex_
	cp	':' ; for 0...9
	jr	nc,dehex1
	sub	'0'
	ld	c,a
	jr	dehex2
dehex1:	or	#20 ; for a...f
	sub	'a'
	add	a,10
	ld	c,a
dehex2:	rlc	c
	rlc	c
	rlc	c
	rlc	c
	inc	hl
	ld	a,(hl)
	cp	0
	jr	z,dehex_
	cp	':' ; for 0...9
	jr	nc,dehex3
	sub	'0'
	or	c
	jr	dehex4
dehex3:	or	#20 ; for a...f
	sub	'a'
	add	a,10
	or	c
dehex4:	inc	hl
	ld	(de),a
	inc	de
	jr	dehex0
dehex_:	pop	bc
	pop	hl
	ret

; Turn 16-bit number into decimal string
; BC - number
; HL - pointer to buffer

d2s:	push	af
	push	bc
	push	de
	push	hl
	ld	a,b
	or	c
	jp	nz,d2s_0
	ld	(hl),'0'
	inc	hl
	ld	(hl),a
	jr	d2s_e
d2s_0:	ex	de,hl
	; -10.000
	ld	hl,#d8f0
	ld	(stmp),hl
	call	d2s_s
	; -1.000
	ld	hl,#fc18
	ld	(stmp),hl
	call	d2s_s
	; -100
	ld	hl,#ff9c
	ld	(stmp),hl
	call	d2s_s
	; -10
	ld	hl,#fff6
	ld	(stmp),hl
	call	d2s_s
	; -1
	ld	hl,#ffff
	ld	(stmp),hl
	call	d2s_s
	ex	de,hl
	ld	(hl),0
	pop	hl
	push	hl
	xor	a
	ld	c,a
d2s_2:	ld	a,(hl)
	or	a
	jr	z,d2s_4
	cp	'0'
	jr	nz,d2s_3
	ld	a,c
	or	a
	jr	nz,d2s_3
	ld	(hl),#20
	inc	hl
	jr	d2s_2
d2s_3:	inc	hl
	inc	c
	jr	d2s_2
d2s_4:	pop	hl
	push	hl
	ld	a,(hl)
	cp	#20
	jr	nz,d2s_e
d2s_5:	push	hl
	inc	hl
	pop	de
	ld	a,(hl)
	ex	de,hl
	ld	(hl),a
	ex	de,hl
	or	a
	jr	nz,d2s_5
	jr	d2s_4
d2s_e:	pop	hl
	pop	de
	pop	bc
	pop	af
	ret
d2s_s:	xor	a
	push	bc
d2s_1:	ld	hl,(stmp)
	pop	bc
	push	bc
	add	hl,bc
	ld	c,a
	ld	a,h
	or	a
	jp	m,d2s_1m
	pop	af
	inc	c
	ld	a,c
	ld	b,h
	ld	c,l
	push	bc
	jp	d2s_1
d2s_1m:	ld	a,c
	pop	bc
	ex	de,hl
	add	a,'0'
d2s_1w:	ld	(hl),a
	inc	hl
	ex	de,hl
	ret

; BC - 16-bit number to print
print_dec:
	push	hl
	ld	hl,buf
	call	d2s
	ld	c,5Ch
	rst	10h
	pop	hl
	ret

; A - 8-bit number to print
print_hex:
	push	af
	rra
	rra
	rra
	rra
	and	15
	cp	10
	jr	nc,L1
	add	a,'0'
	jr	L2
L1	add	a,'A'-10
L2	ld	c,5Bh
	rst	10h
	pop	af
print_hex1:
	and	15
	cp	10
	jr	nc,L3
	add	a,'0'
	jr	L4
L3	add	a,'A'-10
L4	ld	c,5Bh
	rst	10h
	ret

print_dot:
	ld	a,'.'
	jr	print_char
print_space:
	ld	a,32
print_char:
	ld	c,5Bh
	rst	10h
	ret

print_newline:
	ld	a,13
	ld	c,5Bh
	rst	10h
	ld	a,10
	ld	c,5Bh
	rst	10h
	ret

msg	db	13,10,"INET2",13,10,0
ver	db	"FIRMWARE VERSION: ",0
get	db	"HTTPGET "
url	db	"http://sprinternet.io:8080/?op=resolv&host=myself",0
err	db	" ERROR",13,10,0
msg2	db	"Body size: ",0
msg3	db	"Resolved IP-address of the Host:",13,10,0
stmp	db	0,0,0,0
mem	db	0
pg1	db	0
pg3	db	0
buf4	db	0,0,0,0
buf	ds	256

     ds    256

ENDSP:	db 0
