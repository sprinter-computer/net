; INET1.ASM by Shaos 25-JUL-2021 (PUBLIC DOMAIN)
; SprinterNet repo: https://gitlab.com/sprinter-computer/net
; Hexadecimal print was taken from ISATEST.ASM (January 2021)

	org   #7F00

;EXE-file header:
        dw 5845h     ; EXE Signature
        db 45h       ; Reserved (EXE type)
        db 00h       ; Version of EXE file
        dw 0200h     ; Code offset
        dw 0000h
        dw 0000h     ; Primary loader size or 0 (no primary loader)
        dw 0000h     ; Reserved
        dw 0000h     ; Reserved
        dw 0000h     ; Reserved
        dw START     ; Loading address
        dw START     ; Starting address (register PC)
        dw ENDSP     ; Stack address (register SP)
        ds 490       ; Reserved

PAGE0	EQU	082h
PAGE1	EQU	0A2h
PAGE2	EQU	0C2h
PAGE3	EQU	0E2h

;code of program

START:

; Write welcome message

        ld   hl,msg
        ld   c,5ch
        rst   10h

	ld	bc,023Dh ; 2 pages
	rst	10h
	jp	c,exit
	ld	(mem),a
	ld	bc,003Bh ; index 0 to page 3
	rst	10h
	jp	c,lend
	ld	a,(mem)
	ld	bc,0139h ; index 1 to page 1
	rst	10h
	jp	c,lend

; Print pages identifiers for every window

	in	a,(PAGE0)
	call	print_hex
	call	print_space

	in	a,(PAGE1)
	call	print_hex
	call	print_space

	in	a,(PAGE2)
	call	print_hex
	call	print_space

	in	a,(PAGE3)
	call	print_hex
	call	print_newline

	ld	hl,buf
	ld	bc,#00D0 ; netinit
	rst	10h ; HL saved !!!
	jp	c,lend
	push	af
	push	bc
	ld	c,5Ch ; PCHARS
	rst	10h
	call	print_newline
	ld	hl,ver
	ld	c,5Ch ; PCHARS
	rst	10h
	pop	bc
	pop	af
	push	bc
	call	print_hex1
	ld	a,'.'
	call	print_char
	pop	bc
	push	bc
	ld	a,b
	call	print_hex1
	ld	a,'.'
	call	print_char
	pop	bc
	ld	a,b
	call	print_hex1
	call	print_newline

	ld	hl,get
	ld	c,5Ch ; PCHARS
	rst	10h
	call	print_newline

	ld	iy,url
	ld	hl,ENDSP
	ld	de,10000
	ld	bc,#30D0 ; httpget
	rst	10h
	jp	c,lerr

	ld	hl,ENDSP
	add	hl,bc
	ld	(hl),0

	push	bc
	ld	a,b
	call	print_hex
	pop	bc
	ld	a,c
	call	print_hex
	call	print_newline
	call	print_newline

	ld	hl,ENDSP
loop:
	ld	a,(hl)
	push	hl
	cp	0
	jr	z,loop0
	cp	10
	jr	nz,loop1
	ld	a,13
	call	print_char
	ld	a,10
loop1:	call	print_char
	pop	hl
	inc	hl
	jr	loop
loop0:	call	print_newline

; Wait keypress

lwait:
        ld   c,30h
        rst   10h

lend:
	ld	a,(mem)
	ld	c,3Eh ; FREEMEM
	rst	10h

; Exit program

exit:
        ld   bc,0041h
        rst   10h

; Error BC

lerr:
	push	bc
	ld	a,b
	call	print_hex
	pop	bc
	ld	a,c
	call	print_hex
	ld	hl,err
	ld	c,5Ch ; PCHARS
	rst	10h
	jr	lwait

print_hex:
	push	af
	rra
	rra
	rra
	rra
	and	15
	cp	10
	jr	nc,L1
	add	a,'0'
	jr	L2
L1	add	a,'A'-10
L2	ld	c,5Bh
	rst	10h
	pop	af
print_hex1:
	and	15
	cp	10
	jr	nc,L3
	add	a,'0'
	jr	L4
L3	add	a,'A'-10
L4	ld	c,5Bh
	rst	10h
	ret

print_space:
	ld	a,32
print_char:
	ld	c,5Bh
	rst	10h
	ret

print_newline:
	ld	a,13
	ld	c,5Bh
	rst	10h
	ld	a,10
	ld	c,5Bh
	rst	10h
	ret

msg	db	13,10,"INET1",13,10,0
ver	db	"FIRMWARE VERSION: ",0
get	db	"HTTPGET "
url	db	"http://sprinternet.io:8080/?op=time",0
err	db	" ERROR",13,10,0
mem	db	0
buf	ds	256

     ds    256

ENDSP:	db 0
