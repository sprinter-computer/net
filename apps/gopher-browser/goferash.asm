;; This is Gopher client based on
;; uGophy (c) 2020 Alexander Sharikhin
;; https://github.com/nihirash/zx-net-tools/tree/master/uGophy
;; Thank you, Nihirash ;)

;; Port to Sprinter Computer (GOFERASH.EXE)
;;        (c) 2021 Alexander "Shaos" Shabarshin

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

        org   #7F00

;EXE-file header:
        dw 5845h     ; EXE Signature
        db 45h       ; Reserved (EXE type)
        db 00h       ; Version of EXE file
        dw 0200h     ; Code offset
        dw 0000h
        dw 0000h     ; Primary loader size or 0 (no primary loader)
        dw 0000h     ; Reserved
        dw 0000h     ; Reserved
        dw 0000h     ; Reserved
        dw START     ; Loading address
        dw START     ; Starting address (register PC)
        dw START2    ; Stack address (register SP)
        ds 490       ; Reserved

PAGE0	EQU	082h
PAGE1	EQU	0A2h
PAGE2	EQU	0C2h
PAGE3	EQU	0E2h

START:
	jp	START2

msg     db 13,10,"GOFERASH"
newl    db 13,10,0
err	db	" ERROR",13,10,0
zerox	db	"0x",0
avail	db	" available",13,10
pg1	db	0
pg3	db	0
pgtmp	db	0
mem	db	0
attr	db	7
xcoord	db	0
ycoord	db	0

        ds 128 ; stack before START2

;code of program

START2:

; Write welcome message

        ld   hl,msg
        ld   c,5ch
        rst  10h
        ld   hl,zerox
        ld   c,5ch
        rst  10h
        or   a
        ld   hl,0
        ld   bc,page_buffer
        sbc  hl,bc
        push hl
        ld   a,h
        call print_hex
        ld   a,l
        call print_hex
        ld   hl,avail
        ld   c,5ch
        rst  10h

; Save old identifiers of pages in windows 1 and 3

	in	a,(PAGE1)
	ld	(pg1),a
	in	a,(PAGE3)
	ld	(pg3),a

	ld	bc,023Dh ; allocate 2 pages
	rst	10h
	jp	c,exit
	ld	(mem),a
	ld	bc,003Bh ; index 0 to page 3
	rst	10h
	jp	c,lend
	ld	a,(mem)
	ld	bc,0139h ; index 1 to page 1
	rst	10h
	jp	c,lend

        ld   c,30h
        rst   10h

    call renderHeader

    call wSec

    ld de, path
    ld hl, server
    ld bc, port
    call openPage

    jp showPage

; 1 sec delay
wSec:
    ei
    ld b, 50
wsLp:
    halt
    djnz wsLp

;    include "screen64.asm"

clearScreen:
; FASTCLS - Thanks to Ivan Mak (forth.exe)
fastcls:
   LD D,D
   LD A,   #50
   LD B,B
   LD E,   #20
   LD A,(attr)
   LD D,A
   IN A,(PAGE1)
   EX AF,AF';'
   LD A,   #50
   OUT (PAGE1),A
   LD HL,   #4301
   LD A,   #80
   AND   #80
   INC A
fcloop:
   OUT (#89),A
   LD E,E
   LD (HL),E ; !!!
   LD B,B
   INC L
   OUT (#89),A
   LD E,E
   LD (HL),D ; !!!
   LD B,B
   INC L
   INC L
   INC L
   BIT 7,l
   JR Z,fcloop
   EX AF,AF';'
   OUT (PAGE1),A
   ret

showCursor:
    ret

hideCursor:
    ret

; Print just one symbol
; A - symbol
putC:
    cp 13
    jr z, mvCR
    or a
    cp ' '
    ret c

    push bc
    push de
    push hl

    LD E,A
    LD A,(attr)
    LD D,A
    IN A,(PAGE1)
    EX AF,AF';'
    LD A,#50
    OUT (PAGE1),A
    LD A,(xcoord)
    LD C,A
    INC C
    ADD A,#81
    OUT (#89),A
    LD A,C
    LD (xcoord),A
    LD HL,#4301
    LD A,(ycoord)
    LD C,A
    LD B,0
    ADD HL,BC
    ADD HL,BC
    ADD HL,BC
    ADD HL,BC
    LD (HL),E
    INC HL
    LD (HL),D
    EX AF,AF';'
    OUT (PAGE1),A

    pop hl
    pop de
    pop bc
    ret

mvCR: ; used in render
    ld bc,(xcoord)
    inc b
    ld c,0
    call gotoXY
    ret

; Set console coordinates
; B = row (0..31), C = column (0..79)
gotoXY:
    ld a,c
    ld (xcoord),a
    ld a,b
    ld (ycoord),a
    ret

;    include "keyboard.asm"

; Returns in A key code or zero if key wans't pressed
inkey:
    ld c,31h ; SCANKEY
    rst 10h
    jr nz,inkey1
    xor a
    ret
inkey1:
    ret

findZero:
    ld a, (hl)
    or a
    ret z
    inc hl
    jp findZero

input:
    ld b, 20
    ld c, 0
    call gotoXY

    ld hl, cleanLine
    call printZ64

    ld hl, iBuff
    call findZero
iLp:
    halt
    push hl

    ld b, 20
    ld c, 0
    call gotoXY
    ld hl, iBuff
    call printZ64

    ld a, '_'
    call putC
    ld a, ' '
    call putC

    call inkey

    cp 0
    jr z, iNth
    cp 12
    jr z, iBS
    cp 13
    jr z, iRet

    pop hl
    ld (hl), A
    push hl

    ld de,iBuff+62
    or a ; clear flag C
    sbc hl,de
    ld a,h
    or l
    jr z,iCr

    pop hl
    inc hl
    jr iLp

iBS: pop hl
     push hl
     ld de,iBuff
     or a ; clear flag C
     sbc hl,de
     ld a,h
     or l
     pop hl
     jr z, iLp

     dec hl
     ld (hl), 0
     jr iLp

iCr  pop hl
     xor a
     ld (hl), a
     push hl
iNth pop hl
     jr iLp

cleanIBuff:
    ld bc, 64
    ld hl, iBuff
    ld de, iBuff + 1
    ld (hl),0
    ldir
    ret

iRet:
    pop hl

    ld b, 20
    ld c, 0
    call gotoXY
    ld hl, cleanLine
    call printZ64
    ret

iBuff defs 65

;    include "utils.asm"

; Print zero-terminated string
; HL - string pointer
putStringZ:
printZ64:
    ld a,(hl)
    and a
    ret z

    push hl
    call putC
    pop hl

    inc hl
    jr printZ64

skipHalf64:
    ld b, 63
loop1
    xor a
    or b
    ret z
    ld a, (hl)
    and a
    ret z
    cp 09
    ret z
    inc hl
    dec b
    jr loop1

skipHalf64T:
    ld b, 63
loop2
    xor a
    or b
    ret z
    ld a, (hl)
    and a
    ret z
    cp 13
    ret z
    cp 10
    ret z
    inc hl
    dec b
    jr loop2

printT64:
    ld b, 63
ptlp:
    xor a
    or b
    ret z

    ld a, (hl)

    and a
    ret z
    cp 09
    ret z

    push bc

    push hl
    call putC
    pop hl

    inc hl
    pop bc
    dec b
    jr ptlp

printL64:
    ld b, 63
loop3
    xor a
    or b
    ret z
    ld a, (hl)

    and a
    ret z
    cp #0A
    ret z
    cp #0D
    ret z

    push hl
    push bc
    call putC
    pop bc
    pop hl
    dec b
    inc hl
    jr loop3

; HL - string
; Return: bc - len
getStringLength:
    ld bc, 0
strLnLp
    ld a, (hl)
    and a
    ret z
    inc bc
    inc hl
    jr strLnLp

SkipWhitespace:
    ld a, (hl)

    cp ' '
    ret nz

    inc hl
    jr SkipWhitespace

; DE <= StringZ
; HL => output
atoi:
    ld hl, 0
loop4
    ld a, (de)

    and a
    ret z
    cp 13
    ret z
    cp 9
    ret z

    sub     #30
    ld      c,l
    ld      b,h
    add     hl,hl
    add     hl,hl
    add     hl,bc
    add     hl,hl
    ld      c,a
    ld      b,0
    add     hl,bc

    inc de

    jr      loop4

findEnd:
    ld a,(hl)

    and a
    ret z

    inc hl
    jr findEnd

;;;;;;;;;;;;;;;;;;;;;;;;

; Binary to decimal stuff
; From https://www.msx.org/forum/development/msx-development/32-bit-long-ascii

; Combined routine for conversion of different sized binary numbers into
; directly printable ASCII(Z)-string
; Input value in registers, number size and -related to that- registers to fill
; is selected by calling the correct entry:
;
;  entry  inputregister(s)  decimal value 0 to:
;   B2D8             A                    255  (3 digits)
;   B2D16           HL                  65535   5   "
;   B2D24         E:HL               16777215   8   "
;   B2D32        DE:HL             4294967295  10   "
;   B2D48     BC:DE:HL        281474976710655  15   "
;   B2D64  IX:BC:DE:HL   18446744073709551615  20   "
;
; The resulting string is placed into a small buffer attached to this routine,
; this buffer needs no initialization and can be modified as desired.
; The number is aligned to the right, and leading 0's are replaced with spaces.
; On exit HL points to the first digit, (B)C = number of decimals
; This way any re-alignment / postprocessing is made easy.
; Changes: AF,BC,DE,HL,IX
; P.S. some examples below

; by Alwin Henseler

B2D8:    LD H,0
         LD L,A
B2D16:   LD E,0
B2D24:   LD D,0
B2D32:   LD BC,0
B2D48:   LD IX,0          ; zero all non-used bits
B2D64:   LD (B2DINV),HL
         LD (B2DINV+2),DE
         LD (B2DINV+4),BC
         LD (B2DINV+6),IX ; place full 64-bit input value in buffer
         LD HL,B2DBUF
         LD DE,B2DBUF+1
         LD (HL)," "
B2DFILC  EQU $-1         ; address of fill-character
         LD BC,18
         LDIR            ; fill 1st 19 bytes of buffer with spaces
         LD (B2DEND-1),BC ;set BCD value to "0" & place terminating 0
         LD E,1          ; no. of bytes in BCD value
         LD HL,B2DINV+8  ; (address MSB input)+1
         LD BC,#0909
         XOR A
B2DSKP0: DEC B
         JR Z,B2DSIZ     ; all 0: continue with postprocessing
         DEC HL
         OR (HL)         ; find first byte <>0
         JR Z,B2DSKP0
B2DFND1: DEC C
         RLA
         JR NC,B2DFND1   ; determine no. of most significant 1-bit
         RRA
         LD D,A          ; byte from binary input value
B2DLUS2: PUSH HL
         PUSH BC
B2DLUS1: LD HL,B2DEND-1  ; address LSB of BCD value
         LD B,E          ; current length of BCD value in bytes
         RL D            ; highest bit from input value -> carry
B2DLUS0: LD A,(HL)
         ADC A,A
         DAA
         LD (HL),A       ; double 1 BCD byte from intermediate result
         DEC HL
         DJNZ B2DLUS0    ; and go on to double entire BCD value (+carry!)
         JR NC,B2DNXT
         INC E           ; carry at MSB -> BCD value grew 1 byte larger
         LD (HL),1       ; initialize new MSB of BCD value
B2DNXT:  DEC C
         JR NZ,B2DLUS1   ; repeat for remaining bits from 1 input byte
         POP BC          ; no. of remaining bytes in input value
         LD C,8          ; reset bit-counter
         POP HL          ; pointer to byte from input value
         DEC HL
         LD D,(HL)       ; get next group of 8 bits
         DJNZ B2DLUS2    ; and repeat until last byte from input value
B2DSIZ:  LD HL,B2DEND    ; address of terminating 0
         LD C,E          ; size of BCD value in bytes
         OR A
         SBC HL,BC       ; calculate address of MSB BCD
         LD D,H
         LD E,L
         SBC HL,BC
         EX DE,HL        ; HL=address BCD value, DE=start of decimal value
         LD B,C          ; no. of bytes BCD
         SLA C           ; no. of bytes decimal (possibly 1 too high)
         LD A,"0"
         RLD             ; shift bits 4-7 of (HL) into bit 0-3 of A
         CP "0"          ; (HL) was > 9h?
         JR NZ,B2DEXPH   ; if yes, start with recording high digit
         DEC C           ; correct number of decimals
         INC DE          ; correct start address
         JR B2DEXPL      ; continue with converting low digit
B2DEXP:  RLD             ; shift high digit (HL) into low digit of A
B2DEXPH: LD (DE),A       ; record resulting ASCII-code
         INC DE
B2DEXPL: RLD
         LD (DE),A
         INC DE
         INC HL          ; next BCD-byte
         DJNZ B2DEXP     ; and go on to convert each BCD-byte into 2 ASCII
         SBC HL,BC       ; return with HL pointing to 1st decimal
         RET

AppendB2D:
; Append results of B2D to string at HL
    ex   de, hl ; Get destination into DE
    ld   hl, B2DBUF
    call SkipWhitespace
    ldir
    ex   de, hl ; Get destination into DE
    ret

B2DINV:  DS 8   ; space for 64-bit input value (LSB first)
B2DBUF:  DS 20  ; space for 20 decimal digits
B2DEND:  DB 0   ; space for terminating 0

; include "gopher.asm"

; hl - server
; de - path
; bc - port
openPage:
    ld (srv_ptr),hl
    ld (path_ptr),de
    ld (port_ptr),bc

    ex de,hl
    ld de,hist
    ld bc,322
    ldir

    ld hl, (srv_ptr)
    ld de, (path_ptr)
    ld bc, (port_ptr)
    call makeRequest
    ld hl, page_buffer
    xor a
    ld (hl), a
    ld de, page_buffer + 1
    ld bc, #ffff - page_buffer - 1
    ldir

    ld hl, page_buffer
    call loadData

    xor a
    ld (show_offset), a
    inc a
    ld (cursor_pos), a
    ret

srv_ptr dw 0
path_ptr dw 0
port_ptr dw 0

; HL - domain stringZ
; DE - path stringZ
; BC - port stringZ
makeRequest:
    ld (srv_ptr), hl
    ld (path_ptr), de
    ld (port_ptr), bc

    ld hl, downloading_msg
    call showTypePrint

    ld de, (port_ptr)
    call atoi
    ld b,h
    ld c,l
    ld hl,(srv_ptr)
    call openTcp
    ld hl,(path_ptr)
    ld d,h
    ld e,l
    call getStringLength
    call sendTcp
    ld de,crlf
    ld bc,2
    call sendTcp

    ret

reqErr:
;    ld sp, stack_pointer

    ld hl, connectionError
    call showTypePrint
    call wSec
    xor a
    ld (connectionOpen), a

    jp historyBack ; Let's try back home on one URL :)

; Load data to ram via gopher
; HL - data pointer
; In data_recv downloaded volume
loadData:
    ld (data_pointer), hl
    ld hl, 0
    ld (data_recv), hl
lpLoop:
    call getPacket

    ld a, (connectionOpen)
    and a
    jp z, ldEnd

    ld de, (data_pointer)
    ld hl, (bytes_avail)
    add hl, de
    ;DISPLAY "debug: ", $

    ld a, h
    cp #40
    jr c, lpLoop
; a bit buggy
    ld bc, (bytes_avail)
    ld hl, output_buffer
    ldir
    ld hl, (data_pointer)
    ld de, (bytes_avail)
    add hl, de
    ld (data_pointer), hl
    ld hl, (data_recv)
    add hl, de
    ld (data_recv), hl

    jp lpLoop

ldEnd
    ld hl, 0
    ld (data_pointer), hl
    ret

; Download file via gopher
; HL - filename
downloadData:
    ; TODO: save to DSS
    ret

;dwnLp:
;    call getPacket : ld a, (connectionOpen) : and a : jp z, dwnEnd

;    ld bc, (bytes_avail), hl, output_buffer, a, (fstream) : call fwrite
;    jp dwnLp
;dwnEnd:

openURI:
    call cleanIBuff

    ld b, 19
    ld c, 0
    call gotoXY
    ld hl, cleanLine
    call printZ64
    ld b, 19
    ld c, 0
    call gotoXY
    ld hl, hostTxt
    call printZ64

    call input

    ld a, (iBuff)
    or a
    jp z, backToPage

    ld b, 19
    ld c, 0
    call gotoXY
    ld hl, cleanLine
    call printZ64

    ld hl, iBuff
    ld de, d_host
    ld bc, 65
    ldir

    ld hl, d_host
    ld de, d_path
    ld bc, d_port
    call openPage

    jp showPage


data_pointer    defw #4000
data_recv       defw 0
fstream         defb 0

closed_callback
    xor a
    ld (connectionOpen), a
    ei
    ret

hostTxt   db 'Enter host: ', 0

crlf        defb 13,10, 0

d_path    db '/'
          defs 254
d_host    defs 70
d_port    db '70'
          defs 5

hist            ds 322
connectionOpen  db 0
downloading_msg db 'Downloading...', 0

connectionError db "Issue with making request - trying get back", 0

;    include "render.asm"

showPage:
    xor a
    ld (show_offset), a
    ld (s_half), a
    inc a
    ld (cursor_pos), a
backToPage:
    call renderScreen
    call showCursor
showLp:
controls:
    xor a
    ld (s_show_flag), a

    call inkey
    cp 0
    jr z, showLp 
    cp 'q'
    jp z, pageCursorUp
    cp 'a'
    jp z, pageCursorDown
    cp 13
    jp z, selectItem 
    cp 'b'
    jp z, historyBack
    cp 'o'
    jp z, pageScrollUp
    cp 'p'
    jp z, pageScrollDn
    cp 'n'
    jp z, openURI
    cp ' '
    jp z, toggleHalf

    jp showLp

toggleHalf:
    ld a, (s_half)
    xor #ff
    ld (s_half), a
    jp backToPage

historyBack:
    ld hl, server
    ld de, path
    ld bc, port
    call openPage
    jp showPage

pageCursorUp:
    ld a, (cursor_pos) 
    dec a
    cp 0
    jp z, pageScrollUp

    push af
    call hideCursor
    pop af
    ld (cursor_pos), a
    call showCursor
    jp showLp

pageCursorDown:
    ld a, (cursor_pos)
    inc a
    cp 21
    jp z, pageScrollDn

    push af
    call hideCursor
    pop af
    ld (cursor_pos), a
    call showCursor
    jp showLp

pageScrollDn:
    ld hl, (show_offset)
    ld de, 20
    add hl, de
    ld (show_offset), hl
    ld a, 1
    ld (cursor_pos), a

    jp backToPage

pageScrollUp:
    ld a, (show_offset)
    and a
    jp z, showLp
    ld hl, (show_offset)
    ld de, 20
    or a
    sbc hl, de
    ld (show_offset), hl
    ld a, 20
    ld (cursor_pos), a

    jp backToPage

selectItem:
    ld a, (cursor_pos)
    dec a
    ld b, a
    ld a, (show_offset)
    add a,b
    ld b, a
    call findLine

    ld a, h
    or l
    jp z, showLp

    ld a, (hl)

    cp '1'
    jr z, downPg
    cp '0'
    jr z, downPg
    cp '9'
    jp z, downFl
    cp '7'
    jr z, userInput
    jp showLp

userInput:
    call cleanIBuff
    call input

    call extractInfo
    ld hl, file_buffer
    call findEnd
    ld a, 9
    ld (hl), a
    inc hl
    ex de, hl
    ld hl, iBuff
    ld bc, 64
    ldir

    ld hl, hist
    ld de, path
    ld bc, 322
    ldir

    ld hl, server_buffer
    ld de, file_buffer
    ld bc, port_buffer
    call openPage

    jp showPage

downPg:
    push af
    call extractInfo

    ld hl, hist
    ld de, path
    ld bc, 322
    ldir

    ld hl, server_buffer
    ld de, file_buffer
    ld bc, port_buffer
    call openPage

    pop af

    cp '1'
    jp z,showPage
    cp '0'
    jp z, showText

    jp showLp

downFl:
    call extractInfo
    call clearRing
    call cleanIBuff

    ld hl, file_buffer
    call findFnme
    jp isOpenable
dfl:
    jp backToPage ; for SPECTRANET (no download - fix it)
    ld hl, file_buffer ; ???
    call findFnme
    ld de, iBuff
    ld bc, 65
    ldir

    call input

    ld hl, iBuff
    call showTypePrint

    ld hl, server_buffer
    ld de, file_buffer
    ld bc, port_buffer
    call makeRequest
    ld hl, iBuff
    call downloadData

    call hideCursor
    call showCursor

    jp backToPage

isOpenable:
    ld a, (hl)
    and a
    jr z, checkFile
    push hl
    call pushRing
    pop hl
    inc hl
    jr isOpenable

imgExt  db ".scr", 0
imgExt2 db ".SCR", 0
;    IFNDEF ZX48
;pt3Ext  db ".pt3", 0
;pt3Ext2 db ".PT3", 0
;pt2Ext  db ".pt2", 0
;pt2Ext2 db ".PT2", 0
;    ENDIF

checkFile:
;; Images
    ld hl, imgExt
    call searchRing
    cp 1
    jr z, loadImage
    ld hl, imgExt2
    call searchRing
    cp 1
    jr z, loadImage
    jp dfl
loadImage:
    ld hl, server_buffer
    ld de, file_buffer
    ld bc, port_buffer
    call makeRequest

    ld hl, #4000

    call loadData

    xor a

    out (#fe), a

    ld b, 255
wKey:
    halt
    ld a, (s_show_flag)
    and a
    jr z, wK2
    dec b
;    jp z, startNext
wK2:
    call inkey
    or a
    jr z, wKey
    cp 's'
    jr z, toggleSS
    jp backToPage

toggleSS:
    ld a, (s_show_flag)
    xor #ff
    ld (s_show_flag), a
    and a
;    jp nz, startNext
    jp backToPage

findFnme:
    push hl
    pop de
ffnmlp:
    ld a, (hl)

    cp 0
    jr z, ffnmend
    cp '/'
    jr z, fslsh
    inc hl
    jp ffnmlp
fslsh:
    inc hl
    push hl
    pop de
    jp ffnmlp
ffnmend:
    push de
    pop hl
    ret

showType:
    ld a, (cursor_pos)
    dec a
    ld b, a
    ld a, (show_offset)
    add a,b
    ld b, a

    call findLine

    ld a, h
    or l
    jr z, showTypeUnknown

    ld a, (hl)

    cp 'i'
    jr z, showTypeInfo
    cp '9'
    jr z, showTypeDown
    cp '1'
    jr z, showTypePage
    cp '0'
    jr z, showTypeText
    cp '7'
    jr z, showTypeInput

    jr showTypeUnknown

showTypeInput:
    ld hl, type_inpt
    call showTypePrint
    call showURI
    ret

showTypeText:
    ld hl, type_text
    call showTypePrint
    call showURI
    ret

showTypeInfo:
    ld hl, type_info
    jp showTypePrint

showTypePage:
    ld hl, type_page
    call showTypePrint
    call showURI
    ret

showTypeDown:
    ld hl, type_down
    call showTypePrint
    call showURI
    ret

showURI:
    call extractInfo
    ld hl, server_buffer
    call printZ64
    ld hl, file_buffer
    call printZ64
    ret

showTypeUnknown:
    ld hl, type_unkn
    jp showTypePrint

showTypePrint:
    push hl

    ld b, 21
    ld c, 0
    call gotoXY
    ld hl, cleanLine
    call printZ64
    ld b, 21
    ld c, 0
    call gotoXY
    pop hl
    jp printZ64

renderHeader:
    call clearScreen
    ld a, #1F
    ld (attr), a
    ld bc, 0
    call gotoXY

    ld hl, head
    call printZ64

    ld a, #07
    ld (attr), a
    ret

renderScreen:
    call renderHeader
    ld b, 20
renderLp:
    push bc
    ld a, 20
    sub b
    ld b, a
    ld a, (show_offset)
    add a,b
    ld b, a
    call renderLine
    pop bc
    djnz renderLp
    ret

; b - line number
renderLine:
    call findLine
    ld a, h
    or l
    ret z
    ld a, (hl)
    and a
    ret z
    inc hl
    ld a, (s_half)
    and a
    call nz, skipHalf64
    call printT64
    call mvCR
    ret

; B - line number
; HL - pointer to line(or zero if doesn't find it)
findLine:
    ld hl, page_buffer
fndLnLp:
    ld a, b
    and a
    ret z

    ld a, (hl)

    ; Buffer ends?
    and a
    jr z, fndEnd

    ; New line?
    cp 10
    jr z, fndLnNL

    inc hl
    jp fndLnLp

fndLnNL:
    dec b
    inc hl
    jp fndLnLp
fndEnd:
    ld hl, 0
    ret

extractInfo:
    ld a, (cursor_pos)
    dec a
    ld b, a
    ld a, (show_offset)
    add a,b
    ld b, a

    call findLine

    ld a, h
    or l
    ret z

    call findNextBlock 

    inc hl
    ld de, file_buffer
    call extractCol
    inc hl
    ld de, server_buffer
    call extractCol
    inc hl
    ld de, port_buffer
    call extractCol
    ret

extractCol:
    ld a, (hl)

    cp 0
    jr z, endExtract
    cp 09
    jr z, endExtract
    cp 13
    jr z, endExtract

    ld (de), a
    inc de
    inc hl
    jr extractCol

endExtract:
    xor a
    ld (de), a
    ret

findNextBlock:
    ld a, (hl)

; TAB
    cp 09
    ret z
; New line
    cp 13
    ret z
; End of buffer
    cp 0
    ret z

    inc hl
    jp findNextBlock

s_half          db  0
s_show_flag     db  0
offset_tmp      dw  0
show_offset     db  0
cursor_pos      db  1
head      db " GOFERASH v1.0.0 - Gopher client for Sprinter based on uGophy (c) A.Sharikhin  ",13,0
cleanLine db "                                                                                ",0
playing   db "Playing... Hold <SPACE> to stop!", 0
type_inpt db "User input: ", 0
type_text db "Text file: ", 0
type_info db "Information ", 0
type_page db "Page: ", 0
type_down db "File to download: ", 0
type_unkn db "Unknown type ", 0 

file_buffer defs 255     ; URI path
server_buffer defs 70   ; Host name
port_buffer defs 7      ; Port

end_inf_buff equ $

;    include "textrender.asm"

showText:
    xor a
    ld (show_offset), a
    ld (s_half), a
reRenderText:
    call renderTextScreen
showTxLp:
    call txControls
    halt
    halt
    halt
    halt
    halt
    jp showTxLp

txControls:
    call inkey

    and a
    ret z

    cp 'q'
    jp z, txUp
    cp 'o'
    jp z, txUp
    cp 'a'
    jp z, txDn
    cp 'p'
    jp z, txDn
    cp 'b'
    jp z, justBack
    cp 'n'
    jp z, openURI
    cp 32
    jp z, toggleHalf2
    ret
justBack:
    pop af
    jp historyBack
toggleHalf2:
    pop af
    ld a, (s_half)
    xor #ff
    ld (s_half), a
    jp reRenderText

txUp:
    ld a, (show_offset)
    and a
    ret z

    sub 20
    ld (show_offset), a
    call renderTextScreen
    ret

txDn:
    ld a, (show_offset)
    add a,20
    ld (show_offset), a

    call renderTextScreen
    ret

renderTextScreen:
    call renderHeader
    ld b, 20
loop5
    push bc

    ld a, 20
    sub b
    ld b,a
    ld a, (show_offset)
    add a,b
    ld b,a
    call renderTextLine

    pop bc
    djnz loop5
;    DISPLAY "CRASH HERE?", $
    ret


renderTextLine:
    call findLine

    ld a, h
    or l
    ret z

    ld a, (hl)
    and a
    ret z
    ld a, (s_half)
    and a
    call nz, skipHalf64T
    call printL64
    call mvCR
    ret

;    include "ring.asm"

; Pushes A to ring buffer
pushRing
    push af
    ld b, 32
    ld hl, ring_buffer + 1
    ld de, ring_buffer
ringL
    ld a, (hl)
    ld (de), a
    inc hl
    inc de
    djnz ringL
    pop af
    ld hl, ring_buffer + 31
    ld (hl), a
    ret

; HL - Compare string(null terminated)
; A - 0 NOT Found
;     1 Found
searchRing:
    ld b, 0
    push hl
serlp:
    ld a, (hl)
    inc hl
    inc b
    and a
    jp nz, serlp
    dec b
    pop hl
    push bc
    push hl
SRWork:
    pop hl
    ld de, ring_buffer + 32
srcLp:
    dec de
    djnz srcLp
    pop bc
ringCmpLp:
    push bc
    push af
    ld a, (de)
    ld b, a
    pop af
    ld a, (hl)
    cp b
    pop bc
    ld a, 0
    ret nz
    inc de
    inc hl
    djnz ringCmpLp
    ld a, 1
    ret

clearRing:
    xor a
    ld hl, ring_buffer
    ld de, ring_buffer + 1
    ld bc, 32
    ld (hl), a
    ldir
    ret

ring_buffer db 0,0,0,0,0,0,0,0
            db 0,0,0,0,0,0,0,0
            db 0,0,0,0,0,0,0,0
            db 0,0,0,0,0,0,0,0
            db 0 ; 33

sadr        dw 2,0,0,0 ; 8 bytes (AF_INET, port, IP-addr)

; HL - domain (pointer to string)
; BC - port (word)
openTcp:
    ld a,b
    ld (sadr+2),a
    ld a,c
    ld (sadr+3),a
    ; resolv
    ld bc,#33D0
    rst 10h
    ld hl,sadr+4
    ld (hl),b
    inc hl
    ld (hl),c
    inc hl
    ld (hl),d
    inc hl
    ld (hl),e
    ld a,e
    or d
    or c
    or b
    jr z,tcpError
    ; socket
    ld d,2 ; AF_INET
    ld e,1 ; SOCK_STREAM
    ld bc,#0FD0
    rst 10h
    bit 7,a
    jr nz,tcpError
    ld (sock_fd),a
    ; connect (offset 0 = 2, offset 2 = port, offset 4 = IP)
    ld e,8
    ld hl,sadr
    ld bc,#1ED0
    rst 10h
    jr nc,tcpOk
    ld a,(sock_fd)
    ld bc,#2DD0 ; close socket if error
    rst 10h
    jr tcpError
tcpOk:
    ld a,1
    ld (connectionOpen),a
    ret

; DE - pointer to string
; BC - count
sendTcp:
    ld h,d
    ld l,e
    ld d,b
    ld e,c
    ld a,(sock_fd)
    ld bc,#21D0 ; send
    rst 10h
    jr c,tcpError
    ret

; Get packet to output_buffer (size 2048)
; fill bytes_avail with appropriate number
; finilize if nothing to receive
getPacket:
    ld a,(sock_fd)
    ld hl,output_buffer
    ld de,2048
    ld bc,#24D0
    rst 10h
    jr c,tcpError
    ld hl,bytes_avail
    ld (hl),c
    inc hl
    ld (hl),b
    ld a,b
    or c
    ret nz
fin: ; finilize tcp
    ld a,(sock_fd)
    ld bc,#2DD0 ; close
    rst 10h
    xor a
    ld (connectionOpen),a
    dec a
    ld (sock_fd),a
    ret

; Netword error
tcpError:
    jp  lerr

ip_buffer defs 4
sock_fd db 0
output_buffer defs 2050
bytes_avail dw 0

; Wait keypress

lwait:
        ld   c,30h
        rst   10h

lend:
	ld	a,(pg1)
	out	(PAGE1),a
	ld	a,(pg3)
	out	(PAGE3),a
	ld	a,(mem)
	ld	c,3Eh ; FREEMEM
	rst	10h

; Exit program

exit:
        ld   bc,0041h
        rst   10h

; Error A

lerr:
	call	print_hex
	ld	hl,err
	ld	c,5Ch ; PCHARS
	rst	10h
	jr	lwait

print_hex:
    push af
    rra
    rra
    rra
    rra
    and 15
    cp  10
    jr  nc,L1
    add a,'0'
    jr  L2
L1: add a,'A'-10
L2: ld  c,5Bh
    rst 10h
    pop af
print_hex1:
    and 15
    cp  10
    jr  nc,L3
    add a,'0'
    jr  L4
L3: add a,'A'-10
L4: ld c,5Bh
    rst 10h
    ret

print_space:
    ld a,32
print_char:
    ld c,5Bh
    rst 10h
    ret

print_newline:
    ld a,13
    ld c,5Bh
    rst 10h
    ld a,10
    ld c,5Bh
    rst 10h
    ret

open_lbl db 'Opening connection to ', 0

path    db '/uhello'
        defs 248
server  db 'nihirash.net'
        defs 58
port    db '70'
        defs 5
        db 0
page_buffer:
        db 0

